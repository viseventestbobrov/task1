# Task1

Getting Started
---------------

The easiest way to get started is to clone the repository:

```bash
# Get the latest snapshot
git clone git@bitbucket.org:viseventestbobrov/task1.git

# Change directory
cd task1

# Install NPM dependencies
npm install

# Then simply start the app
gulp
```
