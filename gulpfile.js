var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    uglify = require('gulp-uglify'),
    spritesmith = require("gulp.spritesmith"),
    csso = require('gulp-csso'),
    gulpif = require("gulp-if"),
    imagemin = require('gulp-imagemin'),
    rigger = require('gulp-rigger'),
    styleInject = require('gulp-style-inject'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('browser-sync', ['sass:build', 'fonts:copy', 'images:build', 'html:build'], function () {
    browserSync.init({
        server: {
            baseDir: "./build"
        },
        notify: false
    });
});

gulp.task('sprite:build', function () {
    var spriteData =
        gulp.src('src/img/sprite/*.*')
            .pipe(spritesmith({
                imgName: '../img/sprite.png',
                cssName: 'sprite.min.css',
            }));

    spriteData.img.pipe(gulp.dest('./build/img/'));
    spriteData.css
        .pipe(sourcemaps.init())
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css/'));
});

gulp.task('sass:build', function () {
    return gulp.src('src/sass/*.sass')
        .pipe(sass({
            includePaths: require('node-bourbon').includePaths
        }).on('error', sass.logError))
        .pipe(rename({ suffix: '.min', prefix: '' }))
        .pipe(autoprefixer({ browsers: ['Safari 10.1', 'Chrome 49'], cascade: false }))
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/css'));
});


gulp.task('images:build', () =>
    gulp.src('src/img/*.*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5,
            svgoPlugins: [{ removeViewBox: true }],

        }, { verbose: true }))
        .pipe(gulp.dest('build/img'))
);

gulp.task('html:build', function () {
    gulp.src('src/*.html')
        .pipe(styleInject())
        .pipe(rigger())
        .pipe(gulp.dest('./build'));
});

gulp.task('fonts:copy', function () {
    return gulp.src([
        'src/fonts/**/*.*'])
        .pipe(gulp.dest('build/fonts/'));
});

gulp.task('watch', function () {
    gulp.watch('src/sass/*.sass', ['fonts:copy']);
    gulp.watch('src/sass/*.sass', ['sass:build', 'html:build']);
    gulp.watch('src/img/sprite/*.png', ['sprite:build']);
    gulp.watch('src/img/*.*', ['images:build']);
    gulp.watch('src/**/*.html', ['html:build']);
    gulp.watch('build/css/*.css').on('change', browserSync.reload);
    gulp.watch('build/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'watch']);